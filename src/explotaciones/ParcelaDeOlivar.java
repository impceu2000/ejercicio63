package explotaciones;
import java.math.BigDecimal;



public class ParcelaDeOlivar extends Parcela {
    private double arrobasDeAceite;
    private double precioArroba;

    private ParcelaDeOlivar(String nombreDeLaParcela, int numeroPiesDeArbol, double ingresosTotalesDeLaParcela, double gastosTotalesEnParcela, double beneficioTotalEnParcela, double arrobasDeAceite, double precioArroba) {
        super(nombreDeLaParcela, numeroPiesDeArbol, ingresosTotalesDeLaParcela, gastosTotalesEnParcela, beneficioTotalEnParcela);
        this.arrobasDeAceite = arrobasDeAceite;
        this.precioArroba = precioArroba;
    }

    public ParcelaDeOlivar() {
    }

    public double ingresosTotalesDeLaParcela() {
        return (this.arrobasDeAceite * this.precioArroba);
    }

    public double beneficioTotalEnParcela() {
        return (this.arrobasDeAceite * this.precioArroba) - this.gastosTotalesEnParcela;
    }


    public String toString() {
        return super.toString() +
                "Arrobas de aceite: " + arrobasDeAceite + "\n" +
                "Precio de la arroba de aceite: " + precioArroba + " €" + "\n\n";
    }

    public void setArrobasDeAceite(double arrobasDeAceite) {
        this.arrobasDeAceite = arrobasDeAceite;
    }

    public void setPrecioArroba(double precioArroba) {
        this.precioArroba = precioArroba;
    }

}