package explotaciones;

import java.util.ArrayList;

public class Explotacion {
    ArrayList<Parcela> explotacion = new ArrayList<>();

    public Explotacion() {
    }

    public void annadirParcela(Parcela parcela) {
        explotacion.add(parcela);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Parcela parcela : explotacion) {
            sb.append(parcela + "\n");
        }
        return sb.toString();
    }

}