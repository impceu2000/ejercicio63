import explotaciones.Explotacion;
import explotaciones.ParcelaDeAlmendros;
import explotaciones.ParcelaDeOlivar;
import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        ParcelaDeAlmendros pa1 = new ParcelaDeAlmendros();
        pa1.setNombreDeLaParcela("Parcela 1 de almendros");
        pa1.setNumeroPiesDeArbol(100);
        pa1.setGastosTotalesEnParcela(BigDecimal.valueOf(500));
        pa1.setEscandallo(BigDecimal.valueOf(0.240));
        pa1.setKilosDeAlmendra(BigDecimal.valueOf(1000));
        pa1.setPrecioAlmendraLimpia(BigDecimal.valueOf(4));

        ParcelaDeOlivar po1 = new ParcelaDeOlivar();
        po1.setNombreDeLaParcela("Parcela de olivar");
        po1.setNumeroPiesDeArbol(600);
        po1.setArrobasDeAceite(BigDecimal.valueOf(100));
        po1.setGastosTotalesEnParcela(BigDecimal.valueOf(600));
        po1.setPrecioArroba(BigDecimal.valueOf(36));

        ParcelaDeAlmendros pa2 = new ParcelaDeAlmendros();
        pa2.setNombreDeLaParcela("Parcela 2 de almendros");
        pa2.setNumeroPiesDeArbol(150);
        pa2.setGastosTotalesEnParcela(BigDecimal.valueOf(700));
        pa2.setEscandallo(BigDecimal.valueOf(0.225));
        pa2.setKilosDeAlmendra(BigDecimal.valueOf(1450));
        pa2.setPrecioAlmendraLimpia(BigDecimal.valueOf(4.25));

        Explotacion explotacion = new Explotacion();
        explotacion.annadirParcela(pa1);
        explotacion.annadirParcela(po1);
        explotacion.annadirParcela(pa2);
        System.out.println(explotacion);
    }
}


jar_file = campos.jar
  
jar: compile
	jar -cfm campo.jar Manifest.txt -C bin .

compile: clear
	mkdir -p bin
	find . -name *.java | xargs javac -cp bin -d bin

clear:
	rm -rf *.jar
	rm -rf bin/*
	rm -f $(jar_file)

debug: compile_debug
	cd bin
	jdb -sourcepath ../src

compile_debug: clear
	mkdir -p bin
	find . -name *.java | xargs javac -g -cp bin -d bin

doc:
	rm -rf html
	mkdir html
	find . -name "*.java" | xargs javadoc -d html -encoding utf-8 -charset utf-i8